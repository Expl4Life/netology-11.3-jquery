$(document).ready(function(){
	// Вызовем функцию загрузки данных с сервера и их обработки
	uploadXHR()
	// Установим интервал обновления, каждые 5 минут
	setInterval(uploadXHR, 300000);
	// заранее создаем DATA, куда поместим данные от сервера
	var data;
	// ===== ФУНКЦИЯ ОБРАБОТКИ ДАННЫХ С СЕРВЕРА ========
	function uploadXHR() {
		var xhr = new XMLHttpRequest();
		xhr.open("GET", 'http://university.netology.ru/api/currency', false);
		xhr.send();
		if (xhr.status == 200) {
			data = JSON.parse(xhr.responseText);
			console.log("currency UPLOADED succesfull");
		} else {
	    	$('.error-message').html('Данные по актуальным валютам не были получены от сервера: <br>'+ xhr.status + ': ' + xhr.statusText + '<br>Попробуйте позже');
		}
		// Модуль обработки полученного массива и изменения DOM-дерева
		if (data != undefined) {
			// Меняем массив, чтобы его алфавитный порядок был в DOM-дереве
			data.reverse();
			// Создаем объект, чтобы добавить его к массиву
			var objRUR = {
				NumCode: 643,
				CharCode: "RUR",
				Name: "Российский рубль",
				Nominal: "100",
				Value: 1
			}
			data.push(objRUR);
			// Переменная i понадобится для добавления данных по ID и value в DOM-дереве для каждой валюты
			var i = data.length - 1;
			$(data).each(function(){
				// Добавляем все валюты из массива в DOM в оба select(а)
				$('#RUR1').after('<option class="cur1 currency" id="cur1-'+ this.CharCode +'" value="'+ i +'">' + this.Name + ' (' + this.CharCode + ')' + '</option>');
				$('#RUR2').after('<option class="cur2 currency" id="cur2-'+ this.CharCode +'" value="'+ i +'">' + this.Name + ' (' + this.CharCode + ')' + '</option>');
				i-=1;
			});
			// Удаляем узел по умолчанию из дерева
			$('#RUR1').remove();
			$('#RUR2').remove();
			// Добавляем валюте USD справа атрибут selected
			$('#cur2-USD').attr("selected", "selected");
			// Аналогично делаем с RUR
			$('#cur1-RUR').attr("selected", "selected");
			// Обратно возвращаем массив в исходное состояние
			data.reverse();
		} 
		// 2. Сделаем предварительный расчет с RUR на USD
		if (data != undefined) {
			var rur1 = $('#input1').val();
			var usd2Index = $('.cur2:selected').val();
			var usd2 = data[usd2Index].Value
			$('#input1').val((usd2*100).toFixed(2));
			$('#input2').val(100);
			// Функция добавления информации о валюте
			addCurInfo();
		} 
	}
	// 3. ======== ФУНКЦИЯ РАСЧЕТА ВАЛЮТЫ ==========
	var calcCurrency = function(){
		var cur1Index = $('.cur1:selected').val(),
		cur2Index = $('.cur2:selected').val(),
		rate1 = data[cur1Index].Value,
		rate2 = data[cur2Index].Value,
		val1 = $('#input1').val(),
		val2 = $('#input2').val(),
		focus1 = $('#input1').attr('class'),
		focus2 = $('#input2').attr('class'),
		changed1 = $('#select1').attr('class'),
		changed2 = $('#select2').attr('class'),
		selected1 = $('#select1').attr('data-selected1'),
		selected2 = $('#select2').attr('data-selected2'),
		result,
		rate;
		if (rate1 > rate2) {
			rate = (rate1/rate2).toFixed(2);
			if (focus2 == "focus2" && selected2 == 'data-selected2' || changed1 == 'changed1' && selected2 == 'data-selected2' && focus1 != "focus1") {
				result = (val2/rate).toFixed(2);
				$('#input1').val(result);
				// console.log("event1-1");
			}
			else {
				result = (rate*val1).toFixed(2);
				$('#input2').val(result);
				// console.log('event1-2');
			}
		}
		else if (cur1Index == cur2Index) {	
			if (changed1 == 'changed1' && selected2 == 'data-selected2' || focus2 == "focus2") {
				var result = $('#input2').val();
				$('#input1').val(result);
				// console.log('event2-1');
			} 
			else if (changed2 == "changed2" && selected2 == 'data-selected2' || focus1 == "focus1") {
				var result = $('#input1').val();
				$('#input2').val(result);
				// console.log('event2-2');
			}
			else {
				var result = $('#input1').val();
				$('#input1').val(result);
				$('#input2').val(result);
				// console.log('event2-3');
			}
		}
		else if (rate1 < rate2) {
			rate = (rate2/rate1).toFixed(2);
			if (focus2 == "focus2") {
				result = (val2*rate).toFixed(2);
				$('#input1').val(result);
				// console.log("event3-1");
			} 
			else if(changed1 == 'changed1' && selected2 == 'data-selected2' && focus1 != "focus1") {
				result = (val2*rate).toFixed(2);
				$('#input1').val(result);
				// console.log("event3-2");
			}
			else {
				result = (val1/rate).toFixed(2);
				$('#input2').val(result);
				// console.log('event3-3');
			}
		}
	}
	// ================== ОБРАБОТЧИКИ СОБЫТИЙ ==================
	// 4.Добавим действие добавления и удаления класса и атрибута
	// для input1 и inpu2 на события focus и blur
	//focus для input1
	$('#input1').on('focus', function(){
		if ($(this).val() == 0 || 0.00) {
			$(this).val(" ");
		}
		$(this).addClass('focus1');
		$('#select1').attr('data-selected1', 'data-selected1');
		checkInput();
	});
	//blur для input1
	$('#input1').on('blur', function(){
		if ($(this).val() == " ") {
			$(this).val(0);
		}
		$(this).removeClass('focus1');
		$('#select1').removeClass('changed1');
	});
	//focus для input2
	$('#input2').on('focus', function(){
		if ($(this).val() == 0 || 0.00) {
			$(this).val(" ");
		}
		$(this).addClass('focus2');
		$('#select2').attr('data-selected2', 'data-selected2');
		checkInput();
	});
	//blur для input2
	$('#input2').on('blur', function(){
		if ($(this).val() == " ") {
			$(this).val(0);
		}
		$(this).removeClass('focus2');
		$('#select2').removeClass('changed2');
	});
	// 5.Подключим к событию change на select1 функцию calcCurrency
	$('#select1').on('change', function(){
		addCurInfo();
		$(this).attr('data-selected1','data-selected1');
		$('#select2').removeClass('changed2');
		$(this).addClass('changed1');
		calcCurrency();
	});
	// 6. Подключим к событию change на select2 функцию calcCurrency
	$('#select2').on('change', function(){
		addCurInfo();
		$(this).attr('data-selected2','data-selected2');
		$('#select1').removeClass('changed1');
		$(this).addClass('changed2');
		calcCurrency();
	});
	// 7. передадим функции обработчику события "keyup" и "focus" для поля input1 и input2
	$('#input1').on('keyup', calcCurrency);
	$('#input2').on('keyup', calcCurrency);
	// 8. ======== ФУНКЦИЯ ДОП.ИНФОРМАЦИИ =========
	function addCurInfo(){
		// Вообще, если в будущем планируется добавить валюты
		// то можно создать все объекты в цикле, количество итераций
		// будет равно количеству создаваемых элементов в DOM-дереве.
		// Количество итераций можно передать в качестве аргумента в функцию
		// Тут сделано для двух блоков.
		var cur1Index = $('.cur1:selected').val(),
		cur2Index = $('.cur2:selected').val(),
		nominal1 = data[cur1Index].Nominal;
		nominal2 = data[cur2Index].Nominal;
		code1 = data[cur1Index].NumCode;
		code2 = data[cur2Index].NumCode;
		charCode1 = data[cur1Index].CharCode;
		charCode2 = data[cur2Index].CharCode;
		rurRate1 = data[cur1Index].Value;
		rurRate2 = data[cur2Index].Value;
		$('.nominal1').text("Мин. номинал: " + nominal1);
		$('.nominal2').text("Мин. номинал: " + nominal2);
		$('.cur-code1').text("Код валюты: " + code1);
		$('.cur-code2').text("Код валюты: " + code2);
		if(cur1Index != 0) {
			$('.rur-rate1').text("Курс " + charCode1 +"/RUR: "+ rurRate1);
			$('.rur-rate1').show();
		} else {
			$('.rur-rate1').hide();
		} 

		if (cur2Index !=0) {
			$('.rur-rate2').text("Курс " + charCode2 +"/RUR: " + rurRate2);
			$('.rur-rate2').show();
		} else {
			$('.rur-rate2').hide();
		}
	}
	// 9. Функция проверки ввода символов в input1 и input2, допускаются только цифры 
	function checkInput() {
		if ($('#input1').attr('class') == "focus1"){
			$('#input1').keypress(function(e) {
				var verified = (e.which == 8 || e.which == undefined || e.which == 0) ? null : String.fromCharCode(e.which).match(/[^0-9]/);
				if (verified) {
					e.preventDefault();
				}
			});
		}
		if ($('#input2').attr('class') == "focus2"){
			$('#input2').keypress(function(e) {
				var verified = (e.which == 8 || e.which == undefined || e.which == 0) ? null : String.fromCharCode(e.which).match(/[^0-9]/);
				if (verified) {
					e.preventDefault();
				}
			});
		}
	}
}); // Конец ready.



